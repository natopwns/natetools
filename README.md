# natetools

## Overview

`natetools` is a collection of Bash shell scripts I use at work, as well as some general examples you can copy and modify to create new shell scripts.
