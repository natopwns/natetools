#!/bin/bash
# yad-and-dialog.sh - An example of how to use yad and dialog as a fallback.

# Functions
# ------------------------------------------------------------------------------
check_for_args ()
{
	if [ $1 -eq 0 ]  # check if any args were passed
	then
		echo "  No arguments passed. Starting in GUI mode."
		if ! command -v yad &> /dev/null  # check if yad is not installed
		then
			echo "  yad is not installed. Exiting..."
			return 0
		else
			return 1
		fi
	else
		echo "  Starting in CLI mode."
		return 2
	fi
}

use_yad ()  # put all of your yad instructions here
{
	yad --infobox --text "Here is an example confirmation dialog." --text-align "center" --title "infobox"
}

use_dialog ()  # put all of your dialog instructions here
{
	dialog --yesno "Here is an example confirmation dialog." 8 30
}
# ------------------------------------------------------------------------------

# Execution
# ------------------------------------------------------------------------------
check_for_args $#  # '$#' contains the number of args passed
ui_mode=$?  # store return value of check_for_args to ui_mode
if [ $ui_mode -eq 1 ]
then
	use_yad
elif [ $ui_mode -eq 2 ]
then
	use_dialog
fi
# ------------------------------------------------------------------------------

# Cleanup
# ------------------------------------------------------------------------------
clear
# ------------------------------------------------------------------------------
