#!/bin/bash
# pdf-page-numbers.sh - Adds page numbers to a pdf.

if [ "$1" = "-h" ] || [ "$1" = "" ]
then
	echo "Usage: `basename $0` [pdf file]"
else
	# find number of pages in pdf
	pageNums=$(pdfinfo "$1" | grep "Pages:")
	pageNums=${pageNums##* }
	# construct the groff document
	echo ".nr HM 0.5i" > page_num.ms
	echo ".nr PO 0.2i" >> page_num.ms
	echo ".nr LL 8i" >> page_num.ms
	echo ".ds CH" >> page_num.ms
	echo ".as RH %" >> page_num.ms
	echo ".P1" >> page_num.ms
	for i in $( seq 1 $pageNums )
	do
		echo ".bp" >> page_num.ms
		echo ".tl ''''" >> page_num.ms
	done
	# export groff document to pdf
	groff -ms -T pdf page_num.ms > page_num.pdf
	# stamp page numbers onto original pdf
	pdftk "$1" multistamp page_num.pdf output "${1/.pdf/_numbered.pdf}"
	# compress pdf (only works sometimes)
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH  -dQUIET -sOutputFile="${1/.pdf/_compressed.pdf}" "${1/.pdf/_numbered.pdf}"
fi
