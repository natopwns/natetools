#!/bin/bash
# pdf-protect.sh - Adds a password to a pdf.

if [ "$1" = "-h" ] || [ "$1" = "" ]
then
	echo "Usage: `basename $0` [pdf to protect] [password]"
else
	pdftk "${1}" output "${1/.pdf/ - pw.pdf}" user_pw "${2}"
fi

