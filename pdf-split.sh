#!/bin/bash
# pdf-split.sh - Splits a pdf into two files.

if [ "$1" = "-h" ] || [ "$1" = "" ]
then
	echo "Usage: `basename $0` [pdf file] [page # to split] [total # of pages]"
else
	pdftk "$1" cat 1-$2 output "${1/.pdf/ - File 1.pdf}"
	pdftk "$1" cat ${2}-$3 output "${1/.pdf/ - File 2.pdf}"
fi
